# Installation de Minikube sur Ubuntu 22.04 LTS

## Mise à jour du système
    sudo apt update
    sudo apt upgrade
    sudo apt install apt-transport-https curl wget software-properties-common -y
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
    sudo usermod -aG docker $USER
    docker run hello-world && echo "Docker successfully installed and running"

## Téléchargement et installation de Minikube
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube

    sudo cp minikube /usr/local/bin/minikube
    minikube version
    minikube start --driver=docker

## Téléchargement et installation de Kubectl
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
    kubectl version --client

